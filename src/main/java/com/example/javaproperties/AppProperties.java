package com.example.javaproperties;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * <p>Properties used by class App.</p>
 */
public class AppProperties
{
    //////////////////////////////////////////////////////////////////////////
    // class static fields

    private static final Logger LOGGER = LogManager.getLogger(
            AppProperties.class
    );

    //////////////////////////////////////////////////////////////////////////
    // class properties

    /**
     * <p>mavenPropertyFoo description.</p>
     */
    private String mavenPropertyFoo;

    /**
     * <p>propertyFoo description.</p>
     */
    private int propertyFoo;

    //////////////////////////////////////////////////////////////////////////
    // class properties getters and setters

    /**
     * <p>{@link #mavenPropertyFoo} getter.</p>
     *
     * @return {@link #mavenPropertyFoo}
     */
    public String getMavenPropertyFoo()
    {
        return mavenPropertyFoo;
    }

    /**
     * <p>{@link #mavenPropertyFoo} setter.</p>
     *
     * @param mavenPropertyFoo  <p>Value for {@link #mavenPropertyFoo}.</p>
     */
    public void setMavenPropertyFoo(
            String mavenPropertyFoo
    )
    {
        this.mavenPropertyFoo = mavenPropertyFoo;
    }

    /**
     * <p>{@link #propertyFoo} getter.</p>
     *
     * @return {@link #propertyFoo}
     */
    public int getPropertyFoo()
    {
        return propertyFoo;
    }

    /**
     * <p>{@link #propertyFoo} setter.</p>
     *
     * @param propertyFoo  <p>Value for {@link #propertyFoo}.</p>
     */
    public void setPropertyFoo(
            int propertyFoo
    )
    {
        this.propertyFoo = propertyFoo;
    }

    //////////////////////////////////////////////////////////////////////////
    // class constructors

    /**
     * <p>Parameter-less constructor.</p>
     */
    AppProperties()
    {
    }

    /**
     * <p>Fully specified constructor.</p>
     */
    AppProperties(
            String mavenPropertyFoo,
            int propertyFoo
    )
    {
        this.mavenPropertyFoo = mavenPropertyFoo;
        this.propertyFoo = propertyFoo;
    }

    //////////////////////////////////////////////////////////////////////////
    // instance methods

    /**
     * <p>Load from inputStream.</p>
     *
     * <p>
     * As for {@link Properties}, must use ISO 8859-1 character encoding.
     * Can use Unicode escapes as defined in
     * <a href="https://docs.oracle.com/javase/specs/jls/se8/html/jls-3.html#jls-3.3">The Java Language Specification, section 3.3</a>.
     * </p>
     *
     * @param inputStream  <p>Load from inputStream.</p>
     * @throws IOException  <p>Thrown when loading from inputStream.</p>
     */
    public void load(
            InputStream inputStream
    )
            throws IOException
    {
        LOGGER.debug("Begin load()");

        Properties properties = new Properties();

        String key;

        properties.load(
                inputStream
        );

        key = "App.mavenPropertyFoo";
        if ( properties.containsKey(key) )
        {
            setMavenPropertyFoo(
                    properties.getProperty(
                            key
                    )
            );
        }

        key = "App.propertyFoo";
        if ( properties.containsKey(key) )
        {
            setPropertyFoo(
                    Integer.valueOf(
                            properties.getProperty(
                                    key
                            )
                    )
            );
        }

        LOGGER.debug("End load()");
    }

    /**
     * <p>Store to outputStream.</p>
     *
     * @param outputStream  <p>Store to outputStream.</p>
     * @param comments  <p>Properties comments to include.</p>
     * @throws IOException  <p>Thrown when storing to outputStream.</p>
     */
    public void store(
            OutputStream outputStream,
            String comments
    )
            throws IOException
    {
        LOGGER.debug("Begin store()");

        Properties properties = new Properties();

        properties.setProperty(
                "App.mavenPropertyFoo",
                getMavenPropertyFoo()
        );

        properties.setProperty(
                "App.propertyFoo",
                String.valueOf(
                        getPropertyFoo()
                )
        );

        properties.store(
                outputStream,
                comments
        );

        LOGGER.debug("End store()");
    }
}
