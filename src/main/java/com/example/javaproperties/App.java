package com.example.javaproperties;

import java.io.*;
import java.nio.charset.StandardCharsets;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * <p>Java application for testing.</p>
 */
public class App
{
    //////////////////////////////////////////////////////////////////////////
    // class static fields

    private static final Logger LOGGER = LogManager.getLogger(
            App.class
    );

    //////////////////////////////////////////////////////////////////////////
    // class instance properties

    /**
     * <p>
     * Make this instance instead of static so avoid problems with multiple
     * tests only triggering static block code once.
     * </p>
     */
    private AppProperties appProperties;

    //////////////////////////////////////////////////////////////////////////
    // constructors

    /**
     * <p>Parameter-less constructor.</p>
     *
     * <p>Load App.properties and App-filtered.properties.</p>
     *
     * <p>If System property App.propertiesFilename exists and file exists,
     * then load that file.</p>
     */
    public App()
    {
        //////////////////////////////////////////////////////////////////////
        // load APP_PROPERTIES from resources

        this.appProperties = new AppProperties();

        try
        {
            this.appProperties.load(
                    App
                            .class
                            .getResourceAsStream(
                                    "App.properties"
                            )
            );

            this.appProperties.load(
                    App
                            .class
                            .getResourceAsStream(
                                    "App-filtered.properties"
                            )
            );
        }
        catch (IOException exception)
        {
            throw new RuntimeException(exception);
        }

        //////////////////////////////////////////////////////////////////////
        // print APP_PROPERTIES

        ByteArrayOutputStream byteArrayOutputStream;

        byteArrayOutputStream = new ByteArrayOutputStream();

        try
        {
            this.appProperties.store(
                    byteArrayOutputStream,
                    null
            );

            System.out.println(
                    byteArrayOutputStream
                            .toString(
                                    StandardCharsets.ISO_8859_1.name()
                            )
            );
        }
        catch (IOException exception)
        {
            throw new RuntimeException(exception);
        }

        //////////////////////////////////////////////////////////////////////
        // load APP_PROPERTIES from file

        String filename = System.getProperty(
                "App.propertiesFilename"
        );

        if (filename != null && filename.length() > 0)
        {
            try
            {
                this.appProperties.load(
                        new FileInputStream(
                                filename
                        )
                );
            }
            catch (IOException exception)
            {
                throw new RuntimeException(exception);
            }
        }

        //////////////////////////////////////////////////////////////////////
        // print APP_PROPERTIES

        byteArrayOutputStream = new ByteArrayOutputStream();

        try
        {
            this.appProperties.store(
                    byteArrayOutputStream,
                    null
            );

            System.out.println(
                    byteArrayOutputStream
                            .toString(
                                    StandardCharsets.ISO_8859_1.name()
                            )
            );
        }
        catch (IOException exception)
        {
            throw new RuntimeException(exception);
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // class static methods

    /**
     * <p>Main entry point.</p>
     *
     * @param args  program arguments
     */
    public static void main(
            String[] args
    )
            throws IOException
    {
        LOGGER.debug("Begin main()");

        processArgs(args);

        System.out.println(
                "Hello, world!"
        );

        LOGGER.debug("End main()");
    }

    /**
     * <p>Process arguments.</p>
     *
     * @param args  A {@link String} array.
     */
    public static void processArgs(
            String[] args
    )
    {
        LOGGER.debug("Begin processArgs()");

        for (String arg : args)
        {
            System.out.println(
                    processArg(arg)
            );
        }

        LOGGER.debug("End processArgs()");
    }

    /**
     * <p>Process an argument.</p>
     *
     * @param arg  A {@link String}.
     * @return  "Hello, arg!"
     */
    public static String processArg(
            String arg
    )
    {
        LOGGER.debug("Begin processArg()");

        LOGGER.debug("End processArg()");

        return "Hello, " + arg + "!";
    }
}