package com.example.javaproperties;

import org.junit.jupiter.api.extension.ExtendWith;

import java.lang.annotation.*;

/**
 *
 * <p><a href="https://stackoverflow.com/questions/46846503/properly-set-system-properties-in-junit-5">Properly set (system) properties in JUnit 5</a></p>
 *
 * <p><a href="https://shekhargulati.com/2015/11/26/junit-rule-java-8-repeatable-annotations-clean-tests/">JUnit Rule + Java 8 Repeatable Annotations == Clean Tests</a></p>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@ExtendWith(SystemPropertyExtension.class)
@Repeatable(SystemProperties.class)
public @interface SystemProperty
{

    String key();

    String value();
}