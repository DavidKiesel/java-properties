package com.example.javaproperties;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * <p>AppPropertiesTest unit tests.</p>
 *
 * <p>Test method naming convention:</p>
 *
 * <p>productionMethod_stateUnderTest_expectedBehavior</p>
 */
public class AppPropertiesTest
{
    //////////////////////////////////////////////////////////////////////////
    // class static fields

    private static final Logger LOGGER = LogManager.getLogger(
            AppPropertiesTest.class
    );

    //////////////////////////////////////////////////////////////////////////
    // getMavenPropertyFoo tests

    @Test
    public void getMavenPropertyFoo_normal_normal()
    {
        AppProperties appProperties = new AppProperties(
                "foo",
                123
        );

        Assertions.assertEquals(
                "foo",
                appProperties.getMavenPropertyFoo(),
                "Should get correct mavenPropertyFoo."
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // setMavenPropertyFoo tests

    @Test
    public void setMavenPropertyFoo_normal_normal()
    {
        AppProperties appProperties = new AppProperties();

        appProperties.setMavenPropertyFoo("foo");

        Assertions.assertEquals(
                "foo",
                appProperties.getMavenPropertyFoo(),
                "mavenPropertyFoo should be set correctly."
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // getPropertyFoo tests

    @Test
    public void getPropertyFoo_normal_normal()
    {
        AppProperties appProperties = new AppProperties(
                "foo",
                123
        );

        Assertions.assertEquals(
                123,
                appProperties.getPropertyFoo(),
                "Should get correct propertyFoo."
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // setPropertyFoo tests

    @Test
    public void setPropertyFoo_normal_normal()
    {
        AppProperties appProperties = new AppProperties();

        appProperties.setPropertyFoo(123);

        Assertions.assertEquals(
                123,
                appProperties.getPropertyFoo(),
                "propertyFoo should be set correctly."
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // constructor tests

    @Test
    public void AppProperties_parameterless_normal()
    {
        AppProperties appProperties = new AppProperties();

        Assertions.assertNotNull(
                appProperties,
                "appProperties should be constructed correctly."
        );

        Assertions.assertNull(
                appProperties.getMavenPropertyFoo(),
                "On AppProperties construction, mavenPropertyFoo should be set correctly."
        );

        Assertions.assertEquals(
                0,
                appProperties.getPropertyFoo(),
                "On AppProperties construction, propertyFoo should be set correctly."
        );
    }

    @Test
    public void AppProperties_normal_normal()
    {
        AppProperties appProperties = new AppProperties(
                "foo",
                123
        );

        Assertions.assertNotNull(
                appProperties,
                "appProperties should be constructed correctly."
        );

        Assertions.assertEquals(
                "foo",
                appProperties.getMavenPropertyFoo(),
                "On AppProperties construction, mavenPropertyFoo should be set correctly."
        );

        Assertions.assertEquals(
                123,
                appProperties.getPropertyFoo(),
                "On AppProperties construction, propertyFoo should be set correctly."
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // load tests

    @Test
    public void load_normal_normal()
            throws IOException
    {
        AppProperties appProperties = new AppProperties();

        String string =
                "App.mavenPropertyFoo=foo\n"
                        + "App.propertyFoo=123"
                ;

        InputStream inputStream = new ByteArrayInputStream(
                string.getBytes(
                        StandardCharsets.ISO_8859_1
                )
        );

        appProperties.load(inputStream);

        Assertions.assertEquals(
                "foo",
                appProperties.getMavenPropertyFoo(),
                "On load, mavenPropertyFoo should be set correctly."
        );

        Assertions.assertEquals(
                123,
                appProperties.getPropertyFoo(),
                "On load, propertyFoo should be set correctly."
        );
    }

    //////////////////////////////////////////////////////////////////////////
    // store tests

    @Test
    public void store_normal_normal()
            throws IOException
    {
        AppProperties appProperties = new AppProperties(
                "foo",
                123
        );

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        appProperties.store(
                byteArrayOutputStream,
                "baz"
        );

        int position = 0;
        int newlinePosition;

        byte[] bytes = byteArrayOutputStream.toByteArray();

        LOGGER.debug(
                "\n"
                + new String(
                        bytes,
                        StandardCharsets.ISO_8859_1
                )
        );

        newlinePosition = ArrayUtils.indexOf(
                bytes,
                (byte) 0xa,
                position
        );

        LOGGER.debug(
                "{}, {}",
                position,
                newlinePosition
        );

        Assertions.assertArrayEquals(
                (
                        "#baz"
                ).getBytes(
                        StandardCharsets.ISO_8859_1
                ),
                Arrays.copyOfRange(
                        bytes,
                        position,
                        newlinePosition
                ),
                "store should yield correct bytes for comments."
        );

        position = newlinePosition + 1;

        newlinePosition = ArrayUtils.indexOf(
                bytes,
                (byte) 0xa,
                position
        );

        LOGGER.debug(
                "{}, {}",
                position,
                newlinePosition
        );

        // skip timestamp line...

        position = newlinePosition + 1;

        newlinePosition = ArrayUtils.indexOf(
                bytes,
                (byte) 0xa,
                position
        );

        LOGGER.debug(
                "{}, {}",
                position,
                newlinePosition
        );

        Assertions.assertArrayEquals(
                (
                        "App.mavenPropertyFoo=foo"
                ).getBytes(
                        StandardCharsets.ISO_8859_1
                ),
                Arrays.copyOfRange(
                        bytes,
                        position,
                        newlinePosition
                ),
                "store should yield correct bytes for mavenPropertyFoo."
        );

        position = newlinePosition + 1;

        newlinePosition = ArrayUtils.indexOf(
                bytes,
                (byte) 0xa,
                position
        );

        LOGGER.debug(
                "{}, {}",
                position,
                newlinePosition
        );

        Assertions.assertArrayEquals(
                (
                        "App.propertyFoo=123"
                ).getBytes(
                        StandardCharsets.ISO_8859_1
                ),
                Arrays.copyOfRange(
                        bytes,
                        position,
                        newlinePosition
                ),
                "store should yield correct bytes for propertyFoo."
        );

        Assertions.assertEquals(
                80,
                bytes.length,
                "store should yield correct number of bytes."
        );
    }
}