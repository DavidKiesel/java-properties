package com.example.javaproperties;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * App unit tests.
 *
 * test method naming convention:
 *
 * productionMethod_stateUnderTest_expectedBehavior
 */
public class AppTest 
{
    private static final Logger LOGGER = LogManager.getLogger(
            AppTest.class
    );

    private PrintStream originalSystemOut;
    private ByteArrayOutputStream systemOutContent;

    @BeforeEach
    void redirectSystemOutStream()
    {
        originalSystemOut = System.out;

        systemOutContent = new ByteArrayOutputStream();

        System.setOut(
                new PrintStream(systemOutContent)
        );
    }

    @AfterEach
    void restoreSystemOutStream()
    {
        System.setOut(
                originalSystemOut
        );
    }

    /**
     * <p>method: {@link App#main(String[])}</p>
     *
     * <p>state: App.propertiesFilename not set</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    public void main_normal_normal()
            throws IOException
    {
        String[] args = {
                "Tom"
        };

        App app = new App();

        app.main(args);

        String string = systemOutContent.toString();

        LOGGER.debug(
                string
        );

        LOGGER.debug(
                "{}",
                string.indexOf("\n")
        );

        String[] strings = string.split(
                "\n"
        );

        int stringNum = 0;

        // skip Properties timestamp comment

        ++stringNum;
        Assertions.assertEquals(
                "App.mavenPropertyFoo=test-mavenValueBar",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "App.propertyFoo=123",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        // skip Properties timestamp comment

        ++stringNum;
        Assertions.assertEquals(
                "App.mavenPropertyFoo=test-mavenValueBar",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "App.propertyFoo=123",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, Tom!",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, world!",
                strings[stringNum],
                "System.out should match."
        );

        Assertions.assertEquals(
                10,
                strings.length,
                "Number of lines in System.out should match."
        );
    }

    /**
     * <p>method: {@link App#main(String[])}</p>
     *
     * <p>state: App.propertiesFilename empty</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    @SystemProperty(
            key = "App.propertiesFilename",
            value = ""
    )
    public void main_AppPropertiesFilenameEmpty_normal()
            throws IOException
    {
        String[] args = {
                "Tom"
        };

        App app = new App();

        app.main(args);

        String string = systemOutContent.toString();

        LOGGER.debug(
                string
        );

        LOGGER.debug(
                "{}",
                string.indexOf("\n")
        );

        String[] strings = string.split(
                "\n"
        );

        int stringNum = 0;

        // skip Properties timestamp comment

        ++stringNum;
        Assertions.assertEquals(
                "App.mavenPropertyFoo=test-mavenValueBar",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "App.propertyFoo=123",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        // skip Properties timestamp comment

        ++stringNum;
        Assertions.assertEquals(
                "App.mavenPropertyFoo=test-mavenValueBar",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "App.propertyFoo=123",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, Tom!",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, world!",
                strings[stringNum],
                "System.out should match."
        );

        Assertions.assertEquals(
                10,
                strings.length,
                "Number of lines in System.out should match."
        );
    }

    /**
     * <p>method: {@link App#main(String[])}</p>
     *
     * <p>state: provide alternate properties file in System property App.propertiesFilename</p>
     *
     * <p>expected: use alternate properties file</p>
     */
    @Test
    // note that during tests Maven sets current working directory to project directory
    @SystemProperty(
            key = "App.propertiesFilename",
            value = "src/test/resources/com/example/javaproperties/App-alternate.properties"
    )
    public void main_alternateProperties_useAlternateProperties()
            throws IOException
    {
        String[] args = {
                "Tom"
        };

        App app = new App();

        app.main(args);

        String string = systemOutContent.toString();

        LOGGER.debug(
                string
        );

        LOGGER.debug(
                "{}",
                string.indexOf("\n")
        );

        String[] strings = string.split(
                "\n"
        );

        int stringNum = 0;

        // skip Properties timestamp comment

        ++stringNum;
        Assertions.assertEquals(
                "App.mavenPropertyFoo=test-mavenValueBar",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "App.propertyFoo=123",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        // skip Properties timestamp comment

        ++stringNum;
        Assertions.assertEquals(
                "App.mavenPropertyFoo=test-alternate",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "App.propertyFoo=321",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, Tom!",
                strings[stringNum],
                "System.out should match."
        );

        ++stringNum;
        Assertions.assertEquals(
                "Hello, world!",
                strings[stringNum],
                "System.out should match."
        );

        Assertions.assertEquals(
                10,
                strings.length,
                "Number of lines in System.out should match."
        );
    }

    /**
     * <p>method: {@link App#processArgs(String[])}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    public void processArgs_normal_normal() {
        String[] args = {
                "Tom"
        };

        App.processArgs(args);

        Assertions.assertEquals(
                "Hello, Tom!\n",
                systemOutContent.toString(),
                "System.out should match."
        );
    }

    /**
     * <p>method: {@link App#processArg(String)}</p>
     *
     * <p>state: normal</p>
     *
     * <p>expected: normal</p>
     */
    @Test
    public void processArg_normal_normal() {
        String actual = App.processArg(
                "Tom"
        );

        Assertions.assertEquals(
                "Hello, Tom!",
                actual,
                "Should return expected String."
        );
    }
}